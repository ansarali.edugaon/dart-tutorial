void main(List<String> arguments) {
  print('Hello world!');
  print(Calculator().add(12, 23));
  print(Calculator().sub(12, 23));
  print(Calculator().mul(12, 23));
  print(Calculator().div(12, 24));
}

class Calculator{
  int add(int a,int b){
    return a+b;
  }

  int sub(int a,int b){
    return a-b;
  }

  int mul(int a,int b){
    return a*b;
  }

  int div(int a,int b){
    return b~/a;
  }
}